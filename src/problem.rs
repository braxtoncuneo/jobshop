use serde::{
    Serialize,
    Deserialize,
};
use z3::{
    Context,
    Optimize,
    SatResult,
    Model,
    Symbol,
    ast::{
        self,
        Ast,
    },
};

use itertools::Itertools;

use std::{
    rc::{
        Rc,
        Weak,
    },
    cell::{
        RefCell,
    },
    ops::DerefMut,
    collections::{
        HashMap,
        HashSet,
    },
    cmp,
};


#[derive(Debug)]
// Handle for identifying data
pub struct Id {
    name            : String,
    number          : u64,
}



#[derive(Debug)]
pub struct AstInt <'ctx>{
    pub ast : ast::Int<'ctx>,
    pub val : u64,
}


// What gets serialized in for each operation

#[derive(Deserialize, Debug, Clone)]
pub struct OpDesc ( pub String, pub u64, pub Vec<String> );




#[derive(Debug)]
pub struct Op <'ctx> {

    pub id              : Id,
    pub deps            : Vec<Weak<RefCell<Op<'ctx>>>>,
    pub duration        : AstInt<'ctx>,
    pub start_time      : ast::Int<'ctx>,
    pub assignment      : ast::Int<'ctx>,
    pub min_start       : Option<AstInt<'ctx>>,
    pub job_number      : u64,
    pub min_gaps        : HashMap<String,AstInt<'ctx>>

}



#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct SolutionOp ( pub String, pub u64, pub u64 );


impl <'ctx> Op <'ctx> {

    pub fn new(
        context     : &'ctx Context,
        id          : Id,
        duration    : u64,
        job_number  : u64,
    ) -> Self {

        let mut time_name = id.name.clone();
        time_name.push_str("_T");
        let start_time = ast::Int::new_const(context,Symbol::String(time_name));

        let mut mach_name = id.name.clone();
        mach_name.push_str("_M");
        let assignment = ast::Int::new_const(context,Symbol::String(mach_name));


        let duration_ast   = ast::Int::from_u64(context,duration);

        Op {
            id,
            deps: Vec::new(),
            start_time,
            assignment,
            min_start: None,
            duration: AstInt{val: duration, ast: duration_ast},
            job_number,
            min_gaps: HashMap::new()
        }

    }
    
    


    fn find_min_start_helper (&mut self, preds : &mut Vec<Rc<RefCell<Op<'ctx>>>> ) {


        self.deps.iter_mut()
            .map( |x|   x.upgrade().unwrap() )
            .for_each( |x| {
                
                if let Ok(mut dep) = x.try_borrow_mut() {
                    dep.find_min_start_helper(preds);
                };
                preds.push(x);
            });

    }

    fn find_min_start (&mut self, context : &'ctx Context, optimize: bool) {

        if self.min_start.is_some() {
            return;
        }

        if !optimize {
            self.min_start = Some( AstInt { 
                ast: ast::Int::from_u64(&context,0),
                val: 0, 
            });
            return
        }

        let mut preds = Vec::new();

        self.deps.iter()
            .for_each( |dep|  {
                let dep = dep.upgrade().unwrap();
                {
                    let mut d = dep.try_borrow_mut()
                        .expect("Operation dependancies are NOT acyclic! The operation depends upon itself.");
                    d.find_min_start_helper(&mut preds);
                }
                preds.push(dep);
            });

        let mut borrows = Vec::new();
        let min_start : u64 = preds.iter()
            .map( |x| {
                let maybe_borrow = x.try_borrow_mut();
                if maybe_borrow.is_ok() { 
                    //println!("{}",maybe_borrow.as_ref().unwrap().id.name);
                    let val = maybe_borrow.as_ref().unwrap().duration.val;
                    borrows.push(maybe_borrow.unwrap());
                    val
                } else {
                    0
                }
            })
            .sum();

        self.min_start = Some( AstInt { 
            ast: ast::Int::from_u64(&context,min_start),
            val: min_start, 
        });


    }


    fn is_predecessor (&self, op: &Op) -> bool {
        self.deps.iter()
            .any( |dep| {
                let dep = dep.upgrade().unwrap();
                dep .try_borrow_mut()
                    .map(|d| d.is_predecessor(op) )
                    .unwrap_or(true)
            })
    }

    fn is_direct_predecessor (&self, _op: &Op) -> bool {
        self.deps.iter()
            .any( |dep| {
                let dep = dep.upgrade().unwrap();
                let e =dep.try_borrow_mut().is_err();
                e
            })
    }

    

    fn find_min_gap_helper (&mut self, preds : &mut Vec<Rc<RefCell<Op<'ctx>>>> ) -> bool {

        self.deps.iter_mut()
            .map( |x|   x.upgrade().unwrap() )
            .any( |x| {
                let (in_gap, should_add) = 
                if let Ok(mut dep) = x.as_ref().try_borrow_mut() {
                    if dep.find_min_gap_helper(preds) {
                        (true,true)
                    } else {
                        (false,false)
                    }
                } else {
                    (true,false)
                };
                if should_add {
                    preds.push(x);
                }
                in_gap
            })

    }

    

    fn find_min_gap (&mut self, op : &Op, optimize: bool) -> u64 {

        if !optimize {
            return op.duration.val;
        }

        let mut preds = Vec::new();

        self.deps.iter_mut()
            .map( |x|   x.upgrade().unwrap() )
            .for_each( |x| {
                let should_add = 
                if let Ok(mut dep) = x.as_ref().try_borrow_mut() {
                    if dep.find_min_gap_helper(&mut preds) {
                        true
                    } else {
                        false
                    }
                } else {
                    false
                };
                if should_add {
                    preds.push(x);
                }
            });

        let mut borrows = Vec::new();
        let min_gap : u64 = preds.iter()
            .map( |x| {
                let maybe_borrow = x.try_borrow_mut();
                if maybe_borrow.is_ok() { 
                    //println!("{}",maybe_borrow.as_ref().unwrap().id.name);
                    let val = maybe_borrow.as_ref().unwrap().duration.val;
                    borrows.push(maybe_borrow.unwrap());
                    val
                } else {
                    0
                }
            })
            .sum();

        min_gap + op.duration.val

    }



    fn to_solution_op (&self, model: &Model) -> SolutionOp{

        let machine = model.eval(&self.assignment)
                        .unwrap()
                        .as_u64()
                        .unwrap();
                        
        let start   = model.eval(&self.start_time)
                        .unwrap()
                        .as_u64()
                        .unwrap();

        SolutionOp ( self.id.name.clone(), start, machine )

    }

}






// What gets serialized in for each job

#[derive(Clone, Deserialize, Debug)]
pub struct JobDesc {

    pub name            : String,
    pub ops             : Vec<OpDesc>,

}


#[derive(Debug)]
pub struct Job <'ctx> {

    pub id              : Id,
    pub ops             : Vec<Rc<RefCell<Op<'ctx>>>>,

}


impl <'ctx> Job <'ctx> {

    fn new (context : &'ctx Context, desc : JobDesc, number: u64, optimize: bool) -> Self {

        let id   = Id{ name: desc.name, number };
        let op_indexer : HashMap<_,_> = desc.ops.iter()
            .enumerate()
            .map(|(i,x)| (x.0.clone(),(i,x)))
            .collect();

        let mut ops : Vec<_> = desc.ops.iter()
            .enumerate()
            .map(|(idx,x)| Op::new(
                context, 
                Id { name: x.0.clone(), number: idx as u64 },
                x.1,
                number,
            ))
            .map(RefCell::new)
            .map(Rc::new)
            .collect();

        for  op_rc in &ops {
            let mut op = op_rc.borrow_mut();
            let (_, op_desc) = op_indexer.get(&op.id.name).unwrap();
            for dep in op_desc.2.iter() {
                let (dep_idx, _ ) = op_indexer.get(dep).unwrap();
                op.deps.push(Rc::downgrade(&ops[*dep_idx]));
            }
        }
        
        ops.iter_mut().for_each(|x| x.borrow_mut().find_min_start(context,optimize));


        ops.iter()
            .tuple_combinations::<(_,_)>()
            .for_each(|(x,y)|{
                let mut x = x.borrow_mut();
                let mut y = y.borrow_mut();
                let gap = x.find_min_gap(y.deref_mut(),optimize);
                x.min_gaps.insert(
                    y.id.name.clone(),
                    AstInt{
                        ast: ast::Int::from_u64(&context,gap),
                        val: gap 
                    }
                );
                let gap = y.find_min_gap(x.deref_mut(),optimize);
                y.min_gaps.insert(
                    x.id.name.clone(),
                    AstInt{
                        ast: ast::Int::from_u64(&context,gap),
                        val: gap 
                    }
                );
            });

        Job {
            id,
            ops,
        }

    }

    fn has_cycles (& self) -> bool {
        self.ops.iter()
            .any(|x| {
                let op = x.borrow_mut();
                op.is_predecessor(&op)
            })
    }


}



#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct SolutionDesc {
    pub runtime         : u64,
    pub operations      : Vec<SolutionOp>,
}

#[derive(Debug, PartialEq)]
pub enum JobShopErr {
    Unsat,
    TimeOut,
}


// What gets serialized in from the file
#[derive(Clone, Deserialize, Debug)]
pub struct ProblemDesc {
    pub machine_count   : u64,
    pub jobs            : Vec<JobDesc>,
}


impl ProblemDesc {

    #[allow(unused)]
    pub fn op_name (job_name : &str, number : u64) -> String {
        format!("{}_op{}",job_name,number)
    }

    #[allow(unused)]
    pub fn job_name (number : u64) -> String {
        format!("job{}",number)
    }

    #[allow(unused)]
    pub fn random(machine_count: u64, job_count: u64, op_count: u64, runtime_range: u64, dep_prob : f32) -> Self {
        use rand::{Rng, seq::SliceRandom};
    
        let mut rng = rand::thread_rng();
    
        let mut jobs: Vec<_> = (0..job_count)
            .map(ProblemDesc::job_name)
            .map(|name| JobDesc{ name, ops: Vec::new() })
            .collect();
    
        for number in 0..op_count  {
            let job = jobs.choose_mut(&mut rng).unwrap();
            let op_name = ProblemDesc::op_name(&job.name, number);
            let deps : Vec<String> = job.ops.iter()
                .map ( |dep_desc| dep_desc.0.clone() )
                .filter (|_| rand::thread_rng().gen_range(0f32,1f32) < dep_prob )
                .collect();
            let runtime = rand::thread_rng().gen_range(1,runtime_range+1);
            job.ops.push(OpDesc(op_name, runtime, deps ));
        }
    
        Self{
            machine_count,
            jobs,
        }
    
    }

}


#[derive(Debug)]
// Internal representation of problem, converted from ProblemDesc
pub struct Problem <'ctx> {
    pub jobs            : Vec<Job<'ctx>>,
    pub context         : &'ctx Context,
    pub machine_count   : AstInt<'ctx>,
    pub min_time        : AstInt<'ctx>,
    pub max_time        : AstInt<'ctx>,
    pub start           : ast::Int<'ctx>,
    pub end             : ast::Int<'ctx>,
    pub optimize        : bool,
}

impl <'ctx> Problem <'ctx> {

    pub fn new (context: &'ctx Context, desc : ProblemDesc, optimize : bool) -> Result<Self,JobShopErr> {
        
        // Optimization: floor machine_count to number of jobs, as that is the maximal
        // number of machines required for the minimal runtime
        let machine_count = if optimize {
            cmp::min(desc.machine_count, desc.jobs.len() as u64)
        } else {
            desc.machine_count
        };

        let machine_count   = AstInt{ 
            ast: ast::Int::from_u64(context,machine_count),
            val: machine_count,
        };
        let start           = ast::Int::from_u64(context,0);
        let end             = ast::Int::new_const(context,"end");


        let mut set = HashSet::new();
        let job_dup = desc.jobs.iter()
            .map(|j| &j.name)
            .any(|n| ! set.insert(n.clone()));

        if job_dup {
            return Err(JobShopErr::Unsat);
        }
        

        set.clear();
        let op_dup = desc.jobs.iter()
            .flat_map(|j| j.ops.iter())
            .map(|o| &o.0 )
            .any(|n| ! set.insert(n.clone()));

        if op_dup {
            return Err(JobShopErr::Unsat);
        }

        let jobs : Vec<Job<'ctx>> = desc.jobs.into_iter()
            .enumerate()
            .map(|(i,d)| Job::new(&context,d,i as u64, optimize) )
            .collect();


        // Optimization: Get upper bound on minimum runtime by greedily
        // assigning jobs as contiguous chunks of operations to each machine
        let (min_time,max_time) = if ( machine_count.val == 0 ) || !optimize { (0,0) }
        else {

            // Get total of operation runtimes for each job
            let mut job_totals : Vec<u64> = jobs.iter()
                .map( |job : &Job<'ctx> | job.ops.iter()
                    .map(|op| op.borrow().duration.val )
                    .sum()
                ).collect();
            
            job_totals.sort();

            // Greedily combine job runtime totals which form the smallest runtime
            // until number of totals is less than or equal to the number of machines
            while job_totals.len() as u64 > machine_count.val {
                let (a,b,s) = job_totals.iter()
                    .enumerate()
                    .tuple_combinations::<(_,_)>()
                    .fold( (0,0,u64::MAX), |(w,x,m),((y,yv),(z,zv))|
                        if  m <= *yv+*zv {
                            (w,x,m)
                        } else {
                            (y,z,*yv+*zv)
                        }
                    );
                let (a,b,s) = if a < b { (a,b,s) } else { (b,a,s) };
                job_totals.remove(b);
                job_totals.remove(a);
                let idx = match job_totals.binary_search(&s) {
                    Ok(idx)  => idx,
                    Err(idx) => idx,
                };
                job_totals.insert(idx,s);
            }
            
            let max = job_totals.iter_mut().max().map(|x| x.clone()).unwrap_or(0);
            let min = job_totals.iter().sum::<u64>()/machine_count.val;
            (min,max)
        };

        let min_time = AstInt{ 
            ast: ast::Int::from_u64(context,min_time),
            val: min_time,
        };

        let max_time = AstInt{ 
            ast: ast::Int::from_u64(context,max_time),
            val: max_time,
        };

        Ok(Problem {
            jobs,
            context,
            machine_count,
            min_time,
            max_time,
            start,
            end,
            optimize,
        })

    }



    fn constrain_op(&self, solver : &mut Optimize, op : &Op){



        // Restricts operation to not happen after the time specified by the 'end' variable
        let no_late_end = &op.start_time.add(&[&op.duration.ast]).le(&self.end);

        // Machine assignment should be non-negative
        let machine_min = &op.assignment.ge(&self.start);

        // Machine assignment should be less than the machine count
        let machine_max = &op.assignment.lt(&self.machine_count.ast);
        
        // Combined machine assignment limits
        let machine_lim = &machine_max.and(&[&machine_min]);



        // let nle_str = format!(
        //     " ( ( {}.start + {} ) <= END )",
        //     op.id.name, 
        //     op.duration.val
        // );
        // let mmin_str = format!(
        //     " ( {}.asgn >= START )",
        //     op.id.name
        // );
        // let mmax_str = format!(
        //     " ( {}.asgn < {} )",
        //     op.id.name,
        //     &self.machine_count
        // );
        // let mlim_str = format!("( {} AND {} )", mmin_str, mmax_str);

        // println!("{}",nle_str);
        // println!("{}",mlim_str);

        solver.assert(no_late_end);
        solver.assert(machine_lim);



        // Optimization: Restricts operation to not be assigned a time prior to the 
        // minimum possible time given operation dependencies
        let no_early_start = &op.start_time.ge(&op.min_start.as_ref().unwrap().ast);
        solver.assert(no_early_start);


        // let nes_str = format!(
        //     " ( {}.start >= {} )",
        //     op.id.name, 
        //     op.min_start.as_ref().unwrap().val
        // );
        // println!("{}",nes_str);

    }


    fn constrain_same_job_op_pair(&self,solver : &mut Optimize, pair : (&Op, &Op) ){

        // Unpack op pair
        let (o1,o2) = pair;


        let pred_pair = if self.optimize {
            ( o1.is_predecessor(o2), o2.is_predecessor(o1) )
        } else {
            ( o1.is_direct_predecessor(o2), o2.is_direct_predecessor(o1) )
        };

        let (o1_gap, o2_gap)     = if self.optimize {
            ( o1.min_gaps.get(&o2.id.name).unwrap() , o2.min_gaps.get(&o1.id.name).unwrap() )
        } else {
            (&o2.duration, &o1.duration)
        };
        
        // Ordering constraints
        let o1_after   = &o1.start_time.ge(&o2.start_time.add(&[&o1_gap.ast]));

        let o1_before  = &o1.start_time.add(&[&o2_gap.ast]).le(&o2.start_time);

        // Combined ordering constraints if neither op depends on the other
        let no_overlap = &o1_before.or(&[&o1_after]);

        // Only allow relative ordering compatible with dependencies

        // Optimization: Enforce ordering between operations which indirectly
        // depend upon one-another
        let constraint = match pred_pair {
            (false, false) => no_overlap,
            (true,  false) => o1_after,
            (false, true ) => o1_before,
            _ => panic!(
                    "Operation dependancies are NOT acyclic! The operation {} depends upon {} and vice-versa.",
                    o1.id.name, o2.id.name
                ),
        };


        // let after_string = format!(
        //     " ( {}.start >= ( {}.start + {} ) )",
        //     o1.id.name, 
        //     o2.id.name, 
        //     o2.duration.val
        // );
        // let before_string = format!(
        //     "( ( {}.start + {} ) <= {}.start )",
        //     o1.id.name, 
        //     o1.duration.val, 
        //     o2.id.name
        // );
        // let overlap_string = format!("( {} OR {} )",before_string,after_string);
        // match pred_pair {
        //     (false, false) => println!("{}",overlap_string),
        //     (true,  false) => println!("{}",after_string),
        //     (false, true ) => println!("{}",before_string),
        //     _ => unreachable!(),
        // };

        solver.assert(constraint);

    }

    

    fn constrain_cross_job_op_pair(&self,solver : &mut Optimize, pair : (&Op, &Op) ){

        // Unpack op pair
        let (o1,o2) = pair;

        let o1_after   = &o1.start_time.ge(&o2.start_time.add(&[&o2.duration.ast]));
        let o1_before  = &o1.start_time.add(&[&o1.duration.ast]).le(&o2.start_time);
        let no_overlap = &o1_before.or(&[&o1_after]);

        // println!("No overlap for op pair ( {} , {} )",o1.id.name, o2.id.name);
                    
        let constraint = &o1.assignment._eq(&o2.assignment).not().or(&[&no_overlap]);

        solver.assert(constraint);

    }


    pub fn solution (&self) -> Result<SolutionDesc,JobShopErr> {

        if self.machine_count.val == 0 {
            return Err(JobShopErr::Unsat);
        }

        // Make our new solver

        if self.jobs.iter().any( |j| j.has_cycles() ) {
            return Err(JobShopErr::Unsat);
        }


        let mut optimize        = Optimize::new(&self.context);

        // Adds reasonable starting & ending bounds to each operation



        self.jobs.iter()
            .flat_map( |x| x.ops.iter() )
            .for_each( |op| self.constrain_op(&mut optimize, &*op.borrow() ));


        // Make sure no operations from the same job happen at the same time
        self.jobs.iter()
            .flat_map(|x| x.ops.iter().tuple_combinations::<(_,_)>() )
            .for_each( |(x,y)| 
                self.constrain_same_job_op_pair(
                    &mut optimize, 
                    ( &mut *x.borrow_mut(), &mut *y.borrow_mut() ) 
                ) 
            );

        // Make sure no operations from different jobs use the same machine at the same time
        self.jobs.iter().tuple_combinations::<(_,_)>()
            .flat_map(|(x,y)| x.ops.iter().cartesian_product(y.ops.iter()) )
            .for_each( |(x,y)| 
                self.constrain_cross_job_op_pair(
                    &mut optimize,
                    ( &mut *x.borrow_mut(), &mut *y.borrow_mut() )
                ) 
            );

        // Optimization: Enforce lower and upper bounds upon runtime
        if self.optimize {
            let less_eq_max = &self.end.le(&self.max_time.ast);
            optimize.assert(less_eq_max);

            let greater_eq_min = &self.end.ge(&self.min_time.ast);
            optimize.assert(greater_eq_min);
        }

        optimize.minimize(&self.end);

        match optimize.check(&[]) {
            SatResult::Unsat    => return Err(JobShopErr::Unsat),
            SatResult::Unknown  => return Err(JobShopErr::TimeOut),
            SatResult::Sat      => (),
        };

        let model = optimize.get_model();

        // Fetch minimized runtime
        let runtime     = model.eval(&self.end).unwrap().as_u64().unwrap();

        
        // Translate operations to a serializer-friendly form
        let mut operations : Vec<SolutionOp> = self.jobs.iter()
            .flat_map(|x| x.ops.iter() )
            .map(|x| 
                x.borrow_mut().to_solution_op(&model)
            ).collect();

        operations.sort_by(|a,b| a.2.cmp(&b.2) );
        operations.sort_by(|a,b| a.1.cmp(&b.1) );

        Ok(SolutionDesc {
            runtime,
            operations,
        })

    }

}






#[cfg(test)]
pub mod test {
    use super::*;
    use z3::Config;

    
    #[test]
    fn basic() {

        let prob_desc = ProblemDesc {
            machine_count: 1,
            jobs: vec![
                JobDesc {
                    name: "job_name".to_string(),
                    ops: vec![
                        OpDesc ( "A".to_string(), 1, vec![ ] ),
                        OpDesc ( "B".to_string(), 2, vec!["A".to_string()]  ),
                        OpDesc ( "C".to_string(), 3, vec!["B".to_string()]  ),
                    ]
                }
            ]
        };

        let soln_desc = Problem::new(
            &Context::new(&Config::new()),
            prob_desc,
            true
        ).solution();


        assert_eq!(
            soln_desc,
            Ok(SolutionDesc {
                runtime: 6,
                operations: vec![
                    SolutionOp ( "A".to_string(), 0, 0 ),
                    SolutionOp ( "B".to_string(), 1, 0 ),
                    SolutionOp ( "C".to_string(), 3, 0 ),
                ]
            })
        );
        
    }

    #[test]
    fn serial() {

        let prob_desc = ron::de::from_str(r#"
            ProblemDesc(
                machine_count: 1,
                jobs: [
                    JobDesc(
                        name: "job_name",
                        ops: [
                            ( "A", 1, [] ),
                            ( "B", 2, ["A"] ),
                            ( "C", 3, ["B"] ),
                        ]
                    )
                ]
            )
        "#).unwrap();

        let true_soln = ron::de::from_str(r#"
            SolutionDesc(
                runtime: 6,
                operations: [
                    ( "A", 0, 0 ),
                    ( "B", 1, 0 ),
                    ( "C", 3, 0 ),
                ]
            )
        "#).unwrap();


        let true_soln = Ok(true_soln);

        let soln_desc = Problem::new(
            &Context::new(&Config::new()),
            prob_desc,
            true
        ).solution();


        assert_eq!(soln_desc, true_soln);
        
    }

    #[test]
    fn tree() {

        let prob_desc = ron::de::from_str(r#"
            ProblemDesc(
                machine_count: 3,
                jobs: [
                    JobDesc(
                        name: "job_name",
                        ops: [
                            ( "A", 1, [] ),
                            ( "B", 1, ["A"] ),
                            ( "C", 1, ["A"] ),
                            ( "D", 1, ["B", "C"] ),
                            ( "E", 1, ["B", "C"] ),
                            ( "F", 1, ["D", "E"] ),
                            ( "G", 1, ["D", "E"] ),
                        ]
                    )
                ]
            )
        "#).unwrap();

        let true_soln = ron::de::from_str(r#"
            SolutionDesc(
                runtime: 7,
                operations: [
                    ( "A", 0, 0 ),
                    ( "B", 1, 0 ),
                    ( "C", 2, 0 ),
                    ( "D", 3, 0 ),
                    ( "E", 4, 0 ),
                    ( "F", 5, 0 ),
                    ( "G", 6, 0 ),
                ]
            )
        "#).unwrap();


        let true_soln = Ok(true_soln);

        let soln_desc = Problem::new(
            &Context::new(&Config::new()),
            prob_desc,
            true
        ).solution();


        assert_eq!(soln_desc, true_soln);
        
    }


    #[test]
    fn multi_job() {

        let prob_desc = ron::de::from_str(r#"
            ProblemDesc(
                machine_count: 3,
                jobs: [
                    JobDesc( name: "W", ops: [ 
                            ( "A", 1, [] ),
                            ( "D", 4, ["A"] ),
                        ] 
                    ),
                    JobDesc( name: "X", ops: [ 
                            ( "C", 2, [] ),
                            ( "F", 3, ["C"] ),
                        ] 
                    ),
                    JobDesc( name: "Y", ops: [ ( "B", 5, [] ) ] ),
                    JobDesc( name: "Z", ops: [ ( "E", 3, [] ) ] ),
                ]
            )
        "#).unwrap();

        let soln_desc = Problem::new(
            &Context::new(&Config::new()),
            prob_desc,
            true
        ).solution();


        
        let target_runtime = 6;

        if let Ok(soln_desc) = soln_desc {
            assert_eq!(soln_desc.runtime, target_runtime);
        } else {
            panic!(
                "Solution runtime does not equal target runtime {}",
                target_runtime
            );
        }
        
    }

    
    fn fuzz(machine_count: u64, job_count: u64, op_count: u64, runtime_range: u64, dep_prob : f32, optimize : bool) {

        let prob_desc = ProblemDesc::random(machine_count,job_count,op_count,runtime_range,dep_prob);

        let mut config = Config::new();
        config.set_timeout_msec(10000);
        let soln_desc = Problem::new(
            &Context::new(&config),
            prob_desc,
            optimize
        ).solution();


        assert_ne!(soln_desc, Err(JobShopErr::Unsat));
        assert_ne!(soln_desc, Err(JobShopErr::TimeOut));
        
    }




    #[test]
    #[ignore]
    fn fuzzy() {
        use rand::Rng;

        for test_number in 1..10 {
            let mc = rand::thread_rng().gen_range(1,5);
            let jc = rand::thread_rng().gen_range(1,8);
            let oc = rand::thread_rng().gen_range(5,15);
            let rr = rand::thread_rng().gen_range(1,10);
            let dp = rand::thread_rng().gen_range(0f32,1f32);
            fuzz(mc,jc,oc,rr,dp,true);
        }
        
    }

    #[test]
    #[ignore]
    fn fuzzy_opt_eq() {
        use rand::Rng;

        for test_number in 1..100 {
            let mc = rand::thread_rng().gen_range(1,5);
            let jc = rand::thread_rng().gen_range(1,8);
            let oc = rand::thread_rng().gen_range(5,15);
            let rr = rand::thread_rng().gen_range(1,10);
            let dp = rand::thread_rng().gen_range(0f32,1f32);
            let prob_desc = ProblemDesc::random(mc,jc,oc,rr,dp);

            let mut config = Config::new();
            config.set_timeout_msec(10000);
            let opt_soln = Problem::new(
                &Context::new(&config),
                prob_desc.clone(),
                true
            ).solution();
            let nopt_soln = Problem::new(
                &Context::new(&config),
                prob_desc,
                false
            ).solution();

            if let ( Ok(opt), Ok(nopt) ) = (& opt_soln, & nopt_soln) {
                assert_eq!(opt.runtime,nopt.runtime);
            }
        }
        
    }

}
