use std::{
    fmt::{self, Display}, 
    time::Duration, 
    ops::Add, 
    iter::Sum,
    io::{
        stdout,
        Write,
    },
};

use z3::{
    Config,
    Context,
};


mod problem;

use problem::{
    Problem,
    ProblemDesc,
    JobShopErr,
};
use structopt::StructOpt;

use rayon::prelude::*;

use cpu_time::{
    ProcessTime,
    ThreadTime,
};

#[derive(Debug,StructOpt)]
struct BenchConfig {
    #[structopt(short)] full_bench      : bool, // 100
    #[structopt(short)] no_optimize     : bool, // 100
    #[structopt(short)] test_count      : usize, // 100
    #[structopt(short)] machine_count   : u64, // 1 - 5
    #[structopt(short)] job_count       : u64, // 1 - 5
    #[structopt(short)] op_count        : u64, // 1 - 10
    #[structopt(short)] runtime_range   : u64, // 10
    #[structopt(short)] dep_probability : f32, // 0.5
    #[structopt(short)] cutoff_time     : u64, // 10000
}

#[derive(Clone, Copy)]
struct Input {
    optimize: bool,
    machine_count: u64,
    job_count: u64,
    op_count: u64,
    runtime_range: u64,
    cutoff_time: u64,
    dep_probability: f32,
}

#[derive(Default)]
struct Output {
    runtime: Duration,
    test_count: usize,
    timeout_count: u32,
    unsat_count: u32,
}

impl Add for Output {
    type Output = Self;
    fn add(mut self, rhs: Self) -> Self {
        self.test_count += rhs.test_count;
        self.runtime += rhs.runtime;
        self.timeout_count += rhs.timeout_count;
        self.unsat_count += rhs.unsat_count;
        self
    }
}

impl Sum for Output {
    fn sum<I: Iterator<Item = Self>>(iter: I) -> Self {
        iter.into_iter().fold(Default::default(), Add::add)
    }
}

impl Display for Output {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let secs = self.runtime.as_secs_f64() / self.test_count as f64;

        match (self.timeout_count, self.unsat_count) {
            (0,0) => write!(f, "{:.6}",secs),
            (toc,usc) => write!(f, "{:.6}(to:{})(us:{})",secs,toc,usc),
        }
    }
}

fn run_single(input: Input) -> Output {
    let prob_desc = ProblemDesc::random(
        input.machine_count,
        input.job_count,
        input.op_count,
        input.runtime_range,
        input.dep_probability,
    );

    let t0 = ThreadTime::now();

    let mut config = Config::new();
    config.set_timeout_msec(input.cutoff_time);

    let soln = Problem::new(
        &Context::new(&config),
        prob_desc,
        input.optimize
    ).unwrap().solution();

    Output {
        test_count: 1,
        runtime: t0.elapsed(),
        timeout_count: matches!(soln, Err(JobShopErr::TimeOut)) as u32,
        unsat_count: matches!(soln, Err(JobShopErr::Unsat)) as u32,
    }
}

fn run_tests(input: Input, count: usize) -> Output {
    rayon::iter::repeatn(input, count)
        .map(run_single)
        .sum()
}

fn main() {
    let opt = BenchConfig::from_args();
    println!("{:#?}", opt);

    if opt.full_bench {
        print!(
            "\n\nBENCH RUN :
            < TEST_COUNT: {}, RUNTIME_RANGE: {}, DEP_PROB: {}, CUTOFF_TIME: {} >
            < MACHINE_COUNT: 1-{}, JOB_COUNT: 1-{}, OP_COUNT: 1-{} >\n\n",
            opt.test_count, opt.runtime_range, opt.dep_probability, opt.cutoff_time,
            opt.machine_count, opt.job_count, opt.op_count
        );

        for optimize in [true,false].iter().copied() {
            for machine_count in 1..(opt.machine_count+1) {
                print!("\n\nOPTIMIZE: {}, MACHINE_COUNT: {}, JOB_COUNT: 1-{}, OP_COUNT: 1-{}\n\n",
                    optimize, machine_count, opt.job_count, opt.op_count
                );

                let mut last_average = 9999f64;

                for job_count in 1..(opt.job_count+1) {
                    if job_count != 1 { print!("\n"); }

                    //for op_count in (1..(opt.op_count/2+1)).map(|x|x*2) {
                    for op_count in (1..opt.op_count+1).map(|x|x) {
                        if op_count != 1 { print!(","); }

                        let output = run_tests(
                            Input {
                                optimize,
                                machine_count,
                                job_count,
                                op_count,
                                runtime_range: opt.runtime_range,
                                cutoff_time: opt.cutoff_time,
                                dep_probability: opt.dep_probability,
                            },
                            opt.test_count,
                        );

                        print!("{}", output);
                        stdout().flush().unwrap();

                        let average = output.runtime.as_secs_f64() / output.test_count as f64;
                        if (average >= 0.2) || (average - last_average > 0.1)  {
                            break;
                        }
                        last_average = average;
                    }
                }
            }
        }
        print!("\n\n\n\n");
    } else {
        let mut timeout_count = 0;
        let mut unsat_count = 0;

        let t0 = ProcessTime::now();
            for test_number in 0..opt.test_count {
            dbg!(test_number);
            
            let prob_desc = ProblemDesc::random(
                opt.machine_count,
                opt.job_count,
                opt.op_count,
                opt.runtime_range,
                opt.dep_probability
            );

            let mut config = Config::new();
            config.set_timeout_msec(opt.cutoff_time);
            let soln_desc = Problem::new(
                &Context::new(&config),
                prob_desc,
                !opt.no_optimize
            ).unwrap().solution();

            match soln_desc {
                Err(JobShopErr::Unsat) => {unsat_count += 1;},
                Err(JobShopErr::TimeOut) => {timeout_count += 1;},
                _ => (),
            };

        }
        
        let runtime = t0.elapsed();

        print!("{}", Output {
            runtime,
            test_count: opt.test_count,
            unsat_count,
            timeout_count,
        });
    }

/*    
    let (in_path, out_path) = paths()
        .expect("Usage:  jobshop [input file path] [output file path]");
    
    let prob_desc: ProblemDesc = ron::de::from_reader(
        File::open(in_path).expect("Input file path could not be read from."),
    ).unwrap();

    let result = Problem::new(
        &Context::new(&Config::new()),
        prob_desc,
    ).solution();

    match result {
        Ok(soln_desc) => {
            ron::ser::to_writer(
                File::create(out_path).expect("Output file path could not be opened."),
                &soln_desc,
            ).unwrap();
        },
        Err(JobShopErr::Unsat)   => {
            println!("Problem is unsatisfiable.")
        },
        Err(JobShopErr::TimeOut) => {
            println!("Job Shop solver timed out. Timeout limit must be increased to determine satisfiability.")
        },
    }
*/
    
}


