use std::{
    fs::File, path::PathBuf,
};

use z3::{
    Config,
    Context,
};

use ron;

mod problem;

use problem::{
    Problem,
    ProblemDesc,
    JobShopErr,
};


use structopt::StructOpt;


#[derive(Debug,StructOpt)]
struct JobShopConfig {
    #[structopt(parse(from_os_str))]
    in_path      : PathBuf, // 100
    #[structopt(parse(from_os_str))]
    out_path     : PathBuf, // 100
    #[structopt(default_value = "5000")]
    timeout      : u64,  // 100
}


fn main() {

    use ron::ser::{
        PrettyConfig
    };

    let opt = JobShopConfig::from_args();
  
    let prob_desc: ProblemDesc = ron::de::from_reader(
        File::open(opt.in_path).expect("Input file path could not be read from."),
    ).unwrap();

    let mut config = Config::new();
    config.set_timeout_msec(opt.timeout);
    let context =Context::new(&config);
    let problem = Problem::new(
        &context,
        prob_desc,
        true
    );
    
    if let Err(x) = problem {
        println!("Problem description invalid.");
        return;
    }

    let problem = problem.unwrap();
    
    let result = problem.solution();

    match result {
        Ok(soln_desc) => {
            let pretty_config = PrettyConfig::new();
            let pretty = ron::ser::to_string_pretty (&soln_desc,pretty_config).unwrap();
            //let file = File::create(opt.out_path).expect("Output file path could not be opened.");
            std::fs::write(opt.out_path,pretty).unwrap();
        },
        Err(JobShopErr::Unsat)   => {
            println!("Problem is unsatisfiable.")
        },
        Err(JobShopErr::TimeOut) => {
            println!("Job Shop solver timed out. Timeout limit must be increased to determine satisfiability.")
        },
    }

    
}



